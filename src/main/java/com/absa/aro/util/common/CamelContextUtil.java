package com.absa.aro.util.common;

import static com.absa.aro.util.common.CommonUtil.*;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.spi.PropertiesComponent;

public final class CamelContextUtil {
	private static CamelContext camelContext = new DefaultCamelContext();

	/**
	 * instantiate camel context if not exist.
	 */
	public static void buildCamelContext() {
		if (isNull(camelContext)) {
			camelContext = new DefaultCamelContext();
		}
	}

	public static void loadCamelContext() {
		buildCamelContext();
		PropertiesComponent properties = camelContext.getPropertiesComponent();
		properties.setLocation("classpath:application.properties");
		// startCamelContext();

	}

	public static CamelContext getCamelContext() {
		buildCamelContext();
		return camelContext;
	}

	public static void startCamelContext() {
		try {
			camelContext.start();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static void registerRouteInCamelContext(RouteBuilder routeBuilder) {
		try {
			camelContext.addRoutes(routeBuilder);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}

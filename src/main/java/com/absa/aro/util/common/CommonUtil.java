package com.absa.aro.util.common;

public final class CommonUtil {

    public static <T>boolean isNull(T t){
        return t==null;
    }
    public static <T>boolean isNotNull(T t){
        return t!=null;
    }
}

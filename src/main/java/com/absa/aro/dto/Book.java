package com.absa.aro.dto;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "",
         propOrder = {
           "title",
           "year",
           "price"
         }
)
@XmlRootElement(name = "book")
public class Book {
  @XmlElement(required = true)
  protected String title;
  /*@XmlElement(required = true)
  protected List<String> author;*/
  protected int year;
  protected double price;

  @Override
  public String toString() {
    return "Book{" +
            "title='" + title + '\'' +
            ", year=" + year +
            ", price=" + price +
            '}';
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  /*public List<String> getAuthor() {
    return author;
  }

  public void setAuthor(List<String> author) {
    this.author = author;
  }*/

  public int getYear() {
    return year;
  }

  public void setYear(int year) {
    this.year = year;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }
}
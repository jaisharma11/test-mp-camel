package com.absa.aro.route;

import org.apache.camel.builder.RouteBuilder;

import com.absa.aro.process.ConsoleLogProcess;
import com.absa.aro.process.SoapProcess;

public class CamelRestEndpoint extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        restConfiguration().component("servlet")
                .endpointProperty("servletName", "CamelHttpTransportServlet");

        System.out.println("Camel Router has been started****");
        rest("/{{app.version}}")
                .get("/{{get.rest.url}}")
                .consumes("application/json").produces("application/json")
                .to("direct:soapcall");

        rest("/{{app.version}}")
                .post("/{{post.rest.url}}")
                .consumes("application/json").produces("application/json")
                .to("direct:soapcall");
                //.to("direct:consolelog");

        

        from("direct:soapcall")
        .process(new ConsoleLogProcess())
        .process(new SoapProcess());
        
        from("direct:consolelog")
                .process(new ConsoleLogProcess());
        
    }
}

package com.absa.aro.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

public class ConsoleLogProcess implements Processor {
    public void process(Exchange exchange) throws Exception {
        String data=exchange.getIn().getBody(String.class);
        System.out.println("Data : "+data);
        exchange.getIn().setBody(data);
    }
}

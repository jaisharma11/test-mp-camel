package com.absa.aro.process;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;


public class SoapProcess  implements Processor{
	@Override
	public void process(Exchange exchange) throws Exception {
	
		String responseString = "";
        String outputString = "";
        String wsURL = "https://api.ke.intra.absaafrica:24000/services/services/Version?wsdl";

        String namespace = "https://api.ke.intra.absaafrica:24000/services/services/Version";
        String operationName = "getVersion";

        URL url = new URL(wsURL);
        URLConnection connection = url.openConnection();
        HttpURLConnection httpConn = (HttpURLConnection)connection;
        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        //con.setRequestMethod("POST");
        //con.setRequestProperty("Content-Type","application/soap+xml; charset=utf-8");

        int num1 = 10;
        int num2 = 15;

        String xmlInput = "<soapenv:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:axis=\"http://axis.apache.org\">\r\n" + 
                     "   <soapenv:Header/>\r\n" + 
                     "   <soapenv:Body>\r\n" + 
                     "      <axis:getVersion soapenv:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\"/>\r\n" + 
                     "   </soapenv:Body>\r\n" + 
                     "</soapenv:Envelope>";
        byte[] buffer = new byte[xmlInput.length()];
        buffer = xmlInput.getBytes();
        bout.write(buffer);
        byte[] b = bout.toByteArray();

        String SOAPAction = namespace+"/"+operationName;



        // Set the appropriate HTTP parameters.
        httpConn.setRequestProperty("Content-Length",
                     String.valueOf(b.length));
        httpConn.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
        httpConn.setRequestProperty("SOAPAction", "");
        httpConn.setRequestMethod("POST");
        httpConn.setDoOutput(true);
        httpConn.setDoInput(true);
        OutputStream out = httpConn.getOutputStream();
        //Write the content of the request to the outputstream of the HTTP Connection.
        out.write(b);
        out.close();
        //Ready with sending the request

        System.out.println("Req : "+out);


        //Read the response.
        InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
        BufferedReader in = new BufferedReader(isr);

        //Write the SOAP message response to a String.
        while ((responseString = in.readLine()) != null) {
               outputString = outputString + responseString;
        }

        System.out.println(" Response :  : "+outputString);

	}

}

package com.absa.aro.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/hello")
@Produces
public class WelcomeController {
	
	@GET
	public String getHello() {
		return "Hello";
	}
}

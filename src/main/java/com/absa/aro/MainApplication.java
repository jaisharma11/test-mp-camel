package com.absa.aro;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.absa.aro.route.CamelRestEndpoint;
import com.absa.aro.util.common.CamelContextUtil;

@ApplicationPath("/data")
public class MainApplication extends Application {
	public MainApplication() {
		try {
			CamelContextUtil.loadCamelContext();
			CamelContextUtil.registerRouteInCamelContext(new CamelRestEndpoint());
			/* CamelContextUtil.registerRouteInCamelContext(new ActiveMqJsonRoute()); */
			System.out.println("Router started...........");
			CamelContextUtil.startCamelContext();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
